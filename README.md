# VS Code Installation Guide for Linux Mint ![Alt text](image.png) 

## Description
This guide provides specific instructions for Linux Mint users on how to remove and reinstall Visual Studio Code (VS Code), a popular code editor. For those unfamiliar with VS Code, it is a free, open-source editor that supports a range of programming languages with powerful features like debugging, Git integration, syntax highlighting, and more. For more information on VS Code, visit the [official website](https://code.visualstudio.com/).



## Visuals
Screenshots of the installation process can be provided upon request. For a complete walkthrough, consider visiting the [VS Code Documentation](https://code.visualstudio.com/docs).

## Installation

### Removing VS Code
To completely remove VS Code and associated configurations from your Linux Mint system, follow these steps:

1. Uninstall the application:
    ```sh
    sudo apt-get purge code
    ```
   
2. Remove additional user data:
    ```sh
    rm -rf ~/.vscode
    rm -rf ~/.config/Code
    ```

3. Optional: Remove Repository and Key:
   If you've added the VS Code repository to your system, you might want to remove it as well:
   ```sh
   sudo rm /etc/apt/sources.list.d/vscode.list
   ```
4. If you've added a signing key, you can remove it using:
   ```sh
   sudo apt-key del <key-id>
   ```
5. Clean Up the Package Cache:
   Finally, you can clean up the local repository of retrieved package files:
   ```sh
   sudo apt-get autoremove
   sudo apt-get autoclean
   ```

### Reinstalling VS Code
To reinstall VS Code, perform the following:

1. Update the package lists:
    ```sh
    sudo apt update
    ```

2. Install necessary dependencies:
    ```sh
    sudo apt install software-properties-common apt-transport-https wget
    ```

3. Add the Microsoft GPG Key and move the Key to the Trusted Store:
    ```sh
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
    sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
    ```

4. Remove the Downloaded Key File:
   ```sh
   rm microsoft.gpg
   ```

5. Add the VS Code repository:
    ```sh
    echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" | sudo tee /etc/apt/sources.list.d/vscode.list
    sudo apt update
    ```
    Replace [arch=amd64] with [arch=arm64] if necessary for your system architecture.

6. Update Package Lists and Install Visual Studio Code:
    ```sh
    sudo apt update
    sudo apt install code
    ```